$(document).ready(function() { 
    $('#book-search').submit(function(e) { 
        e.preventDefault(); 
        var query = $('#book').val(); 
    
        var search_url = 'https://www.googleapis.com/books/v1/volumes?q=' + query 
    
        $.ajax({ 
            type: 'GET', 
            url : search_url, 
            dataType: 'json', 
            success: function(res) {
                
                let author;
                var print = '';
                let item;
                try {
                    for(var i=0; i<res.items.length; i++) {
                        author = '';
                        item = res.items[i].volumeInfo;
                        try {
                            for (var j=0; j<item.authors.length; j++) {
                                
                                if (j+1 == item.authors.length) {
                                    author += item.authors[j];
                                }
                                else {
                                    author += item.authors[j] + ', '
                                }
                                
                            }
                            if (author == '') {
                                author += 'No author';
                            }
                        } catch (error) {
                            console.log(error)
                        }
						
                        console.log(item);
                        print+= '<tr> <th scope="row">' + (i+1) + '</th>';
                        print += '<td> <img src="' + res.items[i].volumeInfo.imageLinks.thumbnail + '" height="auto" width="auto"></td>';
                        print += '<td>' + res.items[i].volumeInfo.title + '</td>';
                        print += '<td>' + author + '</td>';
                        print += '</tr>'
                    }   
                } catch (error) {
                    alert('Tidak ada buku tersebut')
                }
                
                
            $('tbody').empty().append(print);
                
                
            },
            error(res) {
                alert('Tidak ada buku tersebut');
            }

        }) 
    }) 
}) 